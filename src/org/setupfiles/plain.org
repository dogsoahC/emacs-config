#+LaTeX_class: org-plain-latex
#+LaTeX_class_options: [a4, 11pt, xcolor=dvipsnames]

#+LaTeX_header: \usepackage{lmodern}
#+LaTeX_header: \usepackage[T1]{fontenc}
#+LaTeX_header: \usepackage[AUTO]{inputenc}
#+LaTeX_header: \usepackage{graphicx}
#+LaTeX_header: \usepackage{amsmath, amsthm, amssymb}
#+LaTeX_header: \usepackage[table, xcdraw]{xcolor}

% Colorizing links in a nicer way.
#+LaTeX_header: \definecolor{bblue}{HTML}{0645AD}
#+LaTeX_header: \usepackage{hyperref}
#+LaTeX_header: \hypersetup{colorlinks, linkcolor=blue, urlcolor=bblue}

% Moving up the title.
#+LaTeX_header: \usepackage{titling}
#+LaTeX_header: \setlength{\droptitle}{-6em}

#+LaTeX_header: \setlength{\parindent}{0pt}
#+LaTeX_header: \setlength{\parskip}{1em}
#+LaTeX_header: \usepackage[stretch=10]{microtype}
#+LaTeX_header: \usepackage{hyphenat}
#+LaTeX_header: \usepackage{ragged2e}
#+LaTeX_header: \usepackage{subfig} % Subfigures (not needed in Org I think)
#+LaTeX_header: \usepackage{listings} % Code highlighting

% Page geometry
#+LaTeX_header: \usepackage[top=1cm, bottom=1.5cm, left=1cm, right=1cm]{geometry}

% Line spacing
#+LaTeX_header: \renewcommand{\baselinestretch}{1.15}

% support underlining text
#+LaTeX_header: \usepackage{ulem}

% Spacing, titling, text setting.
#+LaTeX_header: \usepackage[explicit]{titlesec}

% Title customization
#+LaTeX_header: \pretitle{\begin{center}\fontsize{20pt}{20pt}\selectfont}
#+LaTeX_header: \posttitle{\par\end{center}}
#+LaTeX_header: \preauthor{\begin{center}\vspace{-6bp}\fontsize{14pt}{14pt}\selectfont}
#+LaTeX_header: \postauthor{\par\end{center}\vspace{-25bp}}

#+LaTeX_header: \predate{\begin{center}\fontsize{12pt}{12pt}\selectfont}
#+LaTeX_header: \postdate{\par\end{center}\vspace{0em}}


% Section/subsection headings:

% disable section numbers
#+LaTeX_header: \setcounter{secnumdepth}{0}

%Section
#+LaTeX_header: \titlespacing\section{0pt}{5pt}{5pt} % left margin, space before section header, space after section header

%Subsection
#+LaTeX_header: \titlespacing\subsection{0pt}{5pt}{-2pt} % left margin, space before subsection header, space after subsection header

%Subsubsection
#+LaTeX_header: \titlespacing\subsubsection{0pt}{5pt}{-2pt} % left margin, space before subsection header, space after subsection header 

% List spacing
#+LaTeX_header: \usepackage{enumitem}
#+LaTeX_header: \setlist{itemsep=-2pt} % or \setlist{noitemsep} to leave space around whole list


# %Section
# #+LaTeX_header: \titleformat{\section} {\Large}{\thesection}{1em}{\textbf{#1}} % Section header formatting
# #+LaTeX_header: \titlespacing\section{0pt}{5pt}{-5pt} % left margin, space before section header, space after section header

# %Subsection
# #+LaTeX_header: \titleformat{\subsection} {\large}{\thesubsection}{1em}{\textbf{#1}}
\titlespacing\subsection{0pt}{5pt}{-5pt} % left margin, space before subsection header, space after subsection header

# %Subsubsection
# #+LaTeX_header: \titleformat{\subsubsection} {\large}{\thesubsubsection}{1em}{#1}
\titlespacing\subsubsection{0pt}{5pt}{-5pt} % left margin, space before subsection header, space after subsection header

